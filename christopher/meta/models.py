import sys
import inspect
import sqlalchemy
# from .database import Base, engine
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, DateTime, update, create_engine
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.sql import func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,scoped_session


def postgres_connect(user, password, db, host, port='5432'):
    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port ,db)

    # The return value of create_engine() is our connection object
    con = create_engine(url, client_encoding='utf-8')
    return con

engine = postgres_connect(
            user='columbus_crud',
            password='',
            db='christopher',
            host='lead-database.c46ckajca651.ap-northeast-2.rds.amazonaws.com'
        )

session = scoped_session(
            sessionmaker(
                autocommit  = False, 
                autoflush   = False, 
                bind        = engine
            )
        )

Base = declarative_base()


# 현재 쓰지는 않지만... 나중을 위해서..
def start_session():
    url_ = 'postgresql://{user}:{password}@{host}:5432/{db}'
    url = url_.format(
            user='columbus_crud',
            password='ChristopherColumbus3515',
            host='lead-database.c46ckajca651.ap-northeast-2.rds.amazonaws.com',
            db='christopher'
        )
    engine = create_engine(url, client_encoding='utf-8')
    session = scoped_session(
                sessionmaker(
                    autocommit  = False, 
                    autoflush   = False,
                    bind        = engine    
                )
            )
    Base.metadata.create_all(engine)
    return session()

class PublicDataList(Base):
    __tablename__   = 'public_data_processes'
    __table_args__  = {'schema' : 'columbus'}

    id                  = Column('id', Integer, primary_key=True)
    sequence            = Column('sequence', Integer, unique=True)
    queue_attempt       = Column('queue_attempt', Boolean, default=False)
    request_attempt     = Column('request_attempt', Boolean, default=False)
    response_code       = Column('response_code', Integer)
    response_body       = Column('response_body', Boolean)
    created_at          = Column('created_at', DateTime(timezone=True), server_default=func.now())
    updated_at          = Column('updated_at', DateTime(timezone=True), onupdate=func.now())

    def __init__(self, 
                    id                  = None,
                    sequence            = None,
                    queue_attempt       = None, 
                    request_attempt     = None,
                    response_code       = None,
                    response_body       = None,
                    created_at          = None,
                    updated_at          = None
                ):
        self.id                     = id
        self.sequence               = sequence
        self.queue_attempt          = queue_attempt
        self.request_attempt        = request_attempt
        self.response_code          = response_code
        self.response_body          = response_body
        self.created_at             = created_at
        self.updated_at             = updated_at


class PublicDataDetail(Base):
    __tablename__   = 'public_data_details'
    __table_args__  = {'schema' : 'columbus'}

    id              = Column('id', Integer, primary_key=True)
    sequence        = Column('sequence', Integer, unique=True)
    domain          = Column('domain', String)
    business_no     = Column('business_no', String)
    res_xml         = Column('res_xml', String)
    res_json        = Column('res_json', JSON)
    item_attr_cnt   = Column('item_attr_cnt', Integer)
    created_at      = Column('created_at', DateTime(timezone=True), server_default=func.now())
    updated_at      = Column('updated_at', DateTime(timezone=True), onupdate=func.now())

    def __init__(self,
                    id              = None,
                    domain          = None,
                    business_no     = None,
                    sequence        = None,
                    res_xml         = None,
                    res_json        = None,
                    item_attr_cnt   = None,
                    created_at      = None,
                    updated_at      = None
                ):
        self.id             = id
        self.sequence       = sequence
        self.domain         = domain
        self.business_no    = business_no
        self.res_xml        = res_xml
        self.res_json       = res_json
        self.item_attr_cnt  = item_attr_cnt
        self.created_at     = created_at
        self.updated_at     = updated_at


if __name__ == '__main__':
    Base.metadata.create_all(bind=engine)
    
    try:
        Base.metadata.create_all(engine)
        classes = []
        for i in inspect.getmembers(sys.modules[__name__], inspect.isclass):
            if '__main__' in str(i):
                classes.append(i[0])
        class_list = ', '.join(classes)
        print("model migration successfully completed! migrated models are :\n==> "+class_list)
    except:
        print("Something went wrong with model migration")
