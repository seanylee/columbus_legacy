from os.path import dirname, abspath
import boto3


# Base Directory Setting
# > This would be the base directory of this package.
BASE_DIR = dirname(dirname(abspath(__file__)))

# SQS Configuration
# > Configuration for Simple Queue Service. 
# > For more information, visit "https://aws.amazon.com/ko/sqs/"
SQS_COLLECT_QUEUE_URL = 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/standard-data-queue'
SQS_RENDER_QUEUE_URL = 'https://sqs.ap-northeast-2.amazonaws.com/929450513035/standard_rendering_queue'

ACCESS_KEY = 'AKIA5QZ4RKKF2PQLTAMH'
SECRET_KEY = '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'
REGION_NAME = 'ap-northeast-2'

# QUEUE_URL='https://sqs.ap-northeast-2.amazonaws.com/456146303610/data-queue'
# ACCESS_KEY='AKIAWUNDQHJ5EP2KR56D'
# SECRET_KEY='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'


class SQS:
    """
    DESC : boto3 를 이용해서 SQS 객체 생성
    """
    def __init__(self, target='collecting'):
        
        if target =='collecting':
            self.__QUEUE_URL    = SQS_COLLECT_QUEUE_URL
        elif target =='rendering':
            self.__QUEUE_URL    = SQS_RENDER_QUEUE_URL

        self.__ACCESS_KEY   = ACCESS_KEY
        self.__SECRET_KEY   = SECRET_KEY
        self.__REGION_NAME  = REGION_NAME

    def sqs(self):
        _sqs = boto3.client(
            'sqs',
            region_name             = self.__REGION_NAME,
            aws_access_key_id       = self.__ACCESS_KEY,
            aws_secret_access_key   = self.__SECRET_KEY
        )
        return (_sqs, self.__QUEUE_URL)




class S3:
    """
    DESC : boto3 를 이용해서 S3 객체 생성
    """
    def __init__(self):
        self.__ACCESS_KEY   = ACCESS_KEY
        self.__SECRET_KEY   = SECRET_KEY
        self.__REGION_NAME  = REGION_NAME

    def s3(self):
        _s3 = boto3.client(
                's3',
                aws_access_key_id       = self.__ACCESS_KEY,
                aws_secret_access_key   = self.__SECRET_KEY
                )
        return _s3

    def s3_resource(self):
        s3 = boto3.resource(
                's3',
                aws_access_key_id       = self.__ACCESS_KEY,
                aws_secret_access_key   = self.__SECRET_KEY
                )
        return s3
