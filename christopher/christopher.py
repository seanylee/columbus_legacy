#-*- coding:utf-8 -*-

import sys, os # Command Line Arguments 를 받는데에만 사용! | path 추출하는데 사용
import random
import string
import psutil

from datetime import datetime
from pytz import timezone

from subprocess import check_output
import subprocess
import argparse
from meta.config import SQS, S3


date = datetime.now(timezone('Asia/Seoul')).strftime('%Y-%m-%d') # 모집 일자
datetime_val = datetime.now(timezone('Asia/Seoul')).strftime('%Y-%m-%d %H:%M:%S')


def randomString(stringLength):
    """
    Generate a random string with the combination of lowercase and uppercase letters    
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(stringLength))


class Ship:
    """
    탐험에 필요한 요소로써 Ship, Sailor, Journal 등장!
    탐험! > 항해용 배 > 선원 모집 (포지션에 따른 recruiting) > 업무(Job) 할당 > 업무 지시 (Job Produce)
    """
    def __init__(self,SailorJob):
        """
        필요한 선원에 대한 요청 (인력 충원 요청)
        """
        self.__SAILORJOB = SailorJob
        self.__ACCEPT_ACTION = ['singlerun','collecting','rendering']
                #self.__ACCEPT_ACTION = ['singlerun','dailyrun','intervalrun', 
                #                                               'nonstoprun', 'stop','kill','status']
        print(('''

                 |    |    |          [Step 1]
                )_)  )_)  )_)         항해를 위한 배가 준비되었습니다.
               )___))___))___)\\       배에 승선할 선원의 모집이 곧 진행될 예정입니다.
              )____)____)_____)\\
            _____|____|____|____\\\\__  모집 선원 포지션 : "{}"
        ----\\                   /-----------------------------------------------
        ^^^ ^^^^^^^^^^^^^^^^^^^^^        ^^^^         ^^             ^^^^
            ^^^^      ^^^^     ^^^    ^^  ^         ^^^^       ^^^
                 ^^^^      ^^^    ^^^^         ^^           ^^^^^^^^^^^
        ''').format(SailorJob).replace('\t',''))


    # =========== 선원 모집 ===========
    def recruiting(self, __SAILORJOB=None, SailorAction=None, ActionDetail=None, **kwargs):

        dict_args = kwargs if kwargs else None # 모집 상세
        # current_path = str(os.getcwd()) # 모집 장소

        # ----------- 모집 일지 -----------
        def recruiting_log(self, event):
            if event == 'undefinedjob':
                journal_ = '[' + datetime_val + '] ' + "'{}' isn't registered as sailor job."

            elif event == 'outoforder':
                journal_ = '[' + datetime_val + '] ' + "'{}' isn't proper action type."

            elif event == 'jobassigned':
                journal_ = ('[' + datetime_val + '] ' + "'{}' Sailor Successfully Assigned! "
                                            + "He's working on '{} - {}' now!")
            else:
                journal_ = 'Unknown'
            return journal_


        # ------------------------ 리쿠르팅 시작 => initializer, producer, consumer ------------------------

        try:
            # -- initializer 모집
            if self.__SAILORJOB.lower() == 'initializer':
                if SailorAction.lower() in self.__ACCEPT_ACTION:
                    pyfile_path = './position/initializer/jobinitializer.py'
                    logfile_path = ('./position/initializer/jobinitializer_log/'
                                                + date + "_initializer.log")

                    # 실행에 앞서 initializer 로그파일 확인
                    if not os.path.exists(logfile_path):
                        with open(logfile_path, 'w'):
                            print("[" + datetime_val + "] " 
                                    + "Log file has been generated at '{}'".format(logfile_path))
                            pass

                    command = (
                        "nohup python3 -u"+" "+pyfile_path              # .py 실>행 영역
                        +" "+SailorAction.lower()+" "+ActionDetail      # Argument 입력 >영역
                        + " >> " + logfile_path)                        # Log 수집 영역 (Append)
                    proc = subprocess.Popen(command,shell=True)

                    print("["+datetime_val+"] "+"start 'initializer' process with pid {}".format(proc.pid))
                    print("["+datetime_val+"] "+"In order to kill the background process : 'kill -9 {}'".format(proc.pid))
                    print(recruiting_log(self, 'jobassigned').format(
                        self.__SAILORJOB[:-1], self.__SAILORJOB, SailorAction))

                else:
                    print(recruiting_log(self, 'outoforder').format(SailorAction))


            # -- producer 모집
            elif self.__SAILORJOB.lower() == 'producer':
                if SailorAction.lower() == 'singlerun':
                    print("["+datetime_val+"] "
                        + 'singlerun action has been depreciated for producer job.' 
                        + 'choose either  "collecting","rendering"')

                if SailorAction.lower() in self.__ACCEPT_ACTION:
                    pyfile_path = './position/producer/{}_jobproducer.py'.format(SailorAction.lower())
                    logfile_path = ('./position/producer/producer_log/{}_jobproducer_log/'.format(
                                    SailorAction.lower()) + date+"_producer.log")

                    # 실행에 앞서 producer 로그파일 확인
                    if not os.path.exists(logfile_path):
                        with open(logfile_path, 'w'):
                            print("[" + datetime_val + "] " 
                                    + "Log file has been generated at '{}'".format(logfile_path))
                            pass

                    command = (
                            "nohup python3 -u"+" "+pyfile_path              # .py 실>행 영역
                            +" "+SailorAction.lower()+" "+ActionDetail      # Argument 입력 >영역
                            + " >> " + logfile_path)                        # Log 수집 영역 (Append)
                    proc = subprocess.Popen(command,shell=True)
                    print("["+datetime_val+"] "+"start 'producer' process with pid {}".format(proc.pid))
                    print("["+datetime_val+"] "+"In order to kill the background process : 'kill -9 {}'".format(proc.pid))
                    print(recruiting_log(self, 'jobassigned').format(
                        self.__SAILORJOB[:-1], self.__SAILORJOB, SailorAction))
                else:
                    print(recruiting_log(self, 'outoforder').format(SailorAction))


        # -- consumer 모집
            elif self.__SAILORJOB.lower() == 'consumer':
                if SailorAction.lower() in self.__ACCEPT_ACTION:
                    worder_id = randomString(10)
                    pyfile_path = './position/consumer/{}_jobconsumer.py'.format(SailorAction.lower())
                    #logfolder_path = ('./position/consumer/jobconsumer_log/' + date)
                    logfile_path = ('./position/consumer/consumer_log/{}_jobconsumer_log/'.format(
                                    SailorAction.lower()) + date + '_consumer_'+worder_id+'.log')

                    # 실행에 앞서 오늘 consumer 디렉토리 확인
                    if not os.path.exists(logfile_path):
                        with open(logfile_path, 'w'):
                            print("["+datetime_val+"] "
                                    + "Log file has been generated at '{}'".format(logfile_path))
                            pass

                    command = (
                            "nohup python3 -u"+" "+pyfile_path              # .py 실>행 영역
                            +" "+SailorAction.lower()+" "+ActionDetail      # Argument 입력 >영역
                            + " >> " + logfile_path)                        # Log 수집 영역 (Append)
                    proc = subprocess.Popen(command,shell=True)
                    print("["+datetime_val+"]"+"start 'consumer' process with pid {}".format(proc.pid))
                    print("["+datetime_val+"]"+"In order to kill the background process : 'kill -9 {}'".format(proc.pid))
                    print(recruiting_log(self, 'jobassigned').format(
                        self.__SAILORJOB[:-1], self.__SAILORJOB, SailorAction))
                else:
                    print(recruiting_log(self, 'outoforder').format(SailorAction))


        # # -- reporter 모집 ==> 상황보고 (TODO 에 추가하)
        # elif self.__SAILORJOB.lower() == '':
        #       if SailorAction.lower() in self.__ACCEPT_ACTION:

        # -- 모집 공고가 나지 않은 포지션
            else:
                fail_journal = recruiting_log(self, 'undefinedjob').format(self.__SAILORJOB)
                print(fail_journal)

        except:
            print(recruiting_log(self, event='unknown'))



# Argument Parser를 사용해서 효율적인 Argument 입력 도움 제공!
parser = argparse.ArgumentParser(

                            description=(
                                    ('''

                      
                                                                
     _ ^ _                     v                  |~         {date}
    '_\\V/ `             v                   |/    w
    ' oX`                    v             / (   (|   \\
       X                                  /( (/   |)  |\\
       X                           ____  ( (/    (|   | )   ,
       X                          |----\\ (/ |    /|   |'\\  /^;
       X.a##a.                    \\---*---Y--+-----+---+--/(
    .aa########a.>>                \\-------*---*--*---*--/
 .a################aa. ~~~~~~~~~~~~~~'~~~~~~~~~~~~~~~~~~'~~~~~~~~~~~~~~~~~~~~~~~~~~
  ______  __    __  .______       __       _______.___________.  ______   .______    __    __   _______ .______      
 /      ||  |  |  | |   _  \     |  |     /       |           | /  __  \  |   _  \  |  |  |  | |   ____||   _  \     
|  ,----'|  |__|  | |  |_)  |    |  |    |   (----`---|  |----`|  |  |  | |  |_)  | |  |__|  | |  |__   |  |_)  |    
|  |     |   __   | |      /     |  |     \   \       |  |     |  |  |  | |   ___/  |   __   | |   __|  |      /     
|  `----.|  |  |  | |  |\  \----.|  | .----)   |      |  |     |  `--'  | |  |      |  |  |  | |  |____ |  |\  \----.
 \______||__|  |__| | _| `._____||__| |_______/       |__|      \______/  | _|      |__|  |__| |_______|| _| `._____|
                                                                                                                     
The project named 'Christopher Columbus' was initially conducted in order to bring efficiency 
into the business data collection process from various sources on the internet.

▶ Data process procedure on christopher:
    [ Initialize ] --> [ Collect ] --> [ Render ]
    For more detail, execute "python3 christopher.py -c" on CLI

▶ COMMAND EXAMPLE
     _______________________________________________________________________________
    |                                                                               |
    | CHRISTOPHER GENERAL COMMAND EXAMPLES :                                        |
    |                                                                               |
    |   python3 christopher.py -h   |  python3 christopher.py --help                |
    |   python3 christopher.py -v   |  python3 christopher.py --version             |
    |   python3 christopher.py -t   |  python3 christopher.py --top_resource        |
    |   python3 christopher.py -s   |  python3 christopher.py --sqs_message         |
    |   python3 christopher.py -r   |  python3 christopher.py --renedered_s3        |
    |   python3 christopher.py -p   |  python3 christopher.py --purge_resource      |
    |                                                                               |
    | CHRISTOPHER EXECUTION COMMAND EXAMPLES (WHO-WHAT-WHEN) :                      |
    |                                                                               |
    |   python3 christopher.py -j initializer -a singlerun -d now                   |
    |   python3 christopher.py -j [ producer | consumer ] -a collecting -d now      |
    |   python3 christopher.py -j [ producer | consumer ] -a rendering -d now       |
    |   python3 christopher.py --job producer --action rendering --actiondetail now |
    |_______________________________________________________________________________|

-----------
'''.format(date=datetime_val)).replace('\t','')),formatter_class=argparse.RawTextHelpFormatter)

group = parser.add_mutually_exclusive_group()
group.add_argument('-v', '--version', action='store_true', help='show currently running version of Christopher')
group.add_argument('-m', '--migration', action='store_true', help='model migration')
group.add_argument('-c', '--christopher', action='store_true', help='Christopher project procedure')
group.add_argument('-p', '--purge_resource', action='store_true', help='wipeout whole resources on SQS or S3')
group.add_argument('-s', '--sqs_message', action='store_true', help='retrieve active queues')
group.add_argument('-r', '--rendered_s3', action='store_true', help='retrieve rendered html file list from s3')
group.add_argument('-t', '--top_resource', action='store_true', help='check currently running resources: cpu, mem, pid')

parser.add_argument('-j','--job', type=str, metavar='', required=False, help=': define sailor\'s job position => ["initializer", "producer", "consumer"]')
parser.add_argument('-a','--action', type=str, metavar='', required=False, help=': define sailor\'s main job => ["singlerun", "collecting", "rendering"]')
parser.add_argument('-d','--actiondetail', type=str, metavar='', required=False, help=': define sailor\'s detail job description => ["now", "n_workers", "n_days"]')
args = parser.parse_args()



if __name__ =='__main__':

    if args.migration:
        subprocess.call(["python", "./meta/models.py"])


    elif args.version:
        print("Christopher v0.0.1 :: Uneedcomms, Inc.")


    elif args.job and args.action and args.actiondetail:
        ship=Ship(SailorJob=args.job)
        ship.recruiting(SailorAction=args.action, ActionDetail=args.actiondetail)

    elif args.sqs_message:
        sqs_cls = SQS()
        sqs_client = sqs_cls.sqs()[0]
        sqs_list = sqs_client.list_queues(QueueNamePrefix='standard')
        
        print("["+datetime_val+"] " + "Here is the list of active queues on AWS SQS")
        
        for enum, url in enumerate(sqs_list['QueueUrls']):
            enum = enum + 1
            delimiter = 'standard'
            queue_name = delimiter + url.split(delimiter)[1]
            queue_url = url

            response = sqs_client.get_queue_attributes(
                QueueUrl=queue_url,
                AttributeNames=['All',]
            )


            created = (datetime
                .fromtimestamp(int(response['Attributes']['CreatedTimestamp']))
                .strftime('%Y-%m-%d %H:%M:%S'))

            modified = (datetime
                .fromtimestamp(int(response['Attributes']['LastModifiedTimestamp']))
                .strftime('%Y-%m-%d %H:%M:%S'))

            queued_msg = response['Attributes']['ApproximateNumberOfMessages']
            queuearn = response['Attributes']['QueueArn']


            queue_detail = ("\n{enum_}] ({name_}) => '{url_}'\n".format(enum_=enum, name_=queue_name, url_=queue_url)
                            + "  ► Queue info: {}\n".format(queuearn)
                            + "  ► Number of messages on queue: {}\n".format(queued_msg)
                            + "  ► Created at: {}\n".format(created)
                            + "  ► Modified at: {}\n".format(modified)
                            )
            print(queue_detail)


    elif args.purge_resource:
        purge_message = input("["+datetime_val+"] "
                                + "Select purge target (Numeric format is accepted)\n"
                                + " 1) SQS purge\n 2) S3 bucket purge\n :")

        if purge_message == '1':
            purge_obejct = 'sqs'
        elif purge_message == '2':
            purge_obejct = 's3'
        else:
            purge_object = None


        if purge_obejct == 'sqs':
            intro_message = input("["+datetime_val+"] "
                                +"Select sqs purge option (Numeric format is accepted)\n"
                                + " 1) collecting queue\n 2) rendering queue\n :")

            # collecting
            if intro_message == '1':
                sqs_cls = SQS()
                proceed = True

            # rendering
            elif intro_message == '2':
                sqs_cls = SQS(target='rendering')
                proceed = True
            else:
                proceed = False

            if proceed == True:
                deletemessage = input("["+datetime_val+"] "
                                        + "Warning!! All queue messages will be deleted.\n"
                                        + "If you wish to continue, please enter 'DELETE'\n :") 

                action = 'confirm' if deletemessage == 'DELETE' else 'abort'
                print("["+datetime_val+"] "+"Message Deletion is aborted") if action == 'abort' else None
         
                if action == 'confirm':
                    sqs, sqs_url = sqs_cls.sqs()
                    sqs.purge_queue(QueueUrl=sqs_url)
                    print("["+datetime_val+"] "+"All messages from the queue are removed completely")
            else:
                print("["+datetime_val+"] "+"Message Purge process is unexpectedly terminated")

        elif purge_obejct == 's3':
            intro_message = input("["+datetime_val+"] "
                                +"Select s3 purge option (Numeric format is accepted)\n"
                                +" 1) Bucket 'uneed-columbus/html_index'\n :")

            s3_cls = S3()
            s3 = s3_cls.s3_resource()
            prefix_ = 'html_index/'
            bucket_ = 'uneed-columbus'

            if intro_message == '1':
                confirm_message = input ("["+datetime_val+"] " 
                                            + 'Whole objects under the bucket "html_index" are about to wide out '
                                            + 'In order to continue this process, plesase type "DELETE".\n :'
                                            )

                if confirm_message == 'DELETE':
                    s3.Bucket(bucket_).objects.filter(Prefix=prefix_).delete()
                    print("["+datetime_val+"] "
                            + 'Whole objects are completely wiped out')

                else: 
                    print("["+datetime_val+"] " 
                            + 'Object delete process is unexpectedly terminated')

            else:
                print("["+datetime_val+"] " 
                        + 'Please provide proper option number')


    elif args.rendered_s3:
        intro_message = input("["+datetime_val+"] " 
                + "Select retrieve option (Numeric format is accepted) \n"
                + " 1) Rendered List Summary \n 2) Rendered File List \n 3) Search by URL\n :")

        s3_cls = S3()
        s3 = s3_cls.s3_resource()


        # rendered list summary
        if intro_message == '1':
            prefix_ = 'html_index/'
            bucket_ = 'uneed-columbus'
            size = sum(1 for _ in s3.Bucket(bucket_).objects.filter(Prefix=prefix_))
            bucket = "["+datetime_val+"] " + 'Bucket : {}'.format(bucket_)
            path = "["+datetime_val+"] " + '"{}" Objects are stored on S3 at "{}"'.format(size, prefix_)
            print(bucket)
            print(path)

        elif intro_message == '2':
            s3 = s3_cls.s3()

            def get_all_object_keys(bucket, prefix, start_after = '', keys = []):
                response = s3.list_objects_v2(
                    Bucket     = bucket,
                    Prefix     = prefix,
                    StartAfter = start_after
                )

                if 'Contents' not in response:
                    return keys

                key_list = response['Contents']
                last_key = key_list[-1]['Key']
                keys.extend(key_list)

                return get_all_object_keys(bucket, prefix, last_key, keys)


            object_keys = get_all_object_keys('uneed-columbus', 'html_index/')

            for obj in object_keys:
                key = obj['Key']
                etag = obj['ETag']
                lastmodified = obj['LastModified']
                size = obj['Size']

                print('key : '+key)
                print('size : '+str(size))
                print(type(lastmodified))
                print(lastmodified)

        # list summary
        elif intro_message == '3':
            print("not ready yet")


    elif args.top_resource:
        
        def get_ip_address():
            try:
                ip = subprocess.check_output(["curl", "ifconfig.me"]).decode('utf-8')
                ip = '{:71}'.format("> Public ip address is '{}'".format(ip))
            except:
                ip = '{:71}'.format('curl: (6) Could not resolve host: ifconfig.me')
            return ip

        def cpu_monitor():
            cpu_count = psutil.cpu_count()
            cpu_perct = psutil.cpu_percent()
            cpu_percts = psutil.cpu_percent(percpu=True)

            cpus = list()
            for i in range(len(cpu_percts)):
                nth_cpu_perct = cpu_percts[i]
                index = i+1
                comment = '|\tCPU {} share :'.format(index).ljust(42) + str(nth_cpu_perct)+' %'
                cpus.append(comment)


            cpu_line = '{:=^71}'.format(' CPU ')
            prt_cpu_total = 'Total CPU: '+'{:58}'.format(str(cpu_perct)+' %')
            prt_cpus = '\t\n'.join(cpus)

            return (cpu_line, prt_cpu_total, prt_cpus)


        def mem_monitor():
            vmem = psutil.virtual_memory()
            swap = psutil.swap_memory()

            prt_vmem_total = 'Total memory size: ' + str(round(vmem.total / 10**9, 2)) + ' GB'
            prt_vmem_avail = 'Available memory size: '.ljust(40) + str(round(vmem.available / 10**9,2)) + ' GB'
            prt_vmem_avperct = 'Running memory percentage: '.ljust(40) + str(vmem.percent) + ' %'
            prt_vmem_used = 'Used memory size: '.ljust(40) + str(round(vmem.used / 10**9,2)) + ' GB'
            prt_vmem_free = 'Free memory size: '.ljust(40) + str(round(vmem.free / 10**9,2)) + ' GB'
            prt_vmem_active = 'Active memory size: '.ljust(40) + str(round(vmem.active / 10**9,2)) + ' GB'
            prt_vmem_inactive = 'Inactive memory size: '.ljust(40) + str(round(vmem.inactive / 10 **9,2)) + ' GB'

            memory_line = '{:=^71}'.format(' MEMORY ')
            prt_vmem_total = '{:67}'.format(prt_vmem_total+'\t')
            prt_vmem_avail = '{:65}'.format('\t'+prt_vmem_avail)
            prt_vmem_avperct = '{:65}'.format('\t'+prt_vmem_avperct)
            prt_vmem_free = '{:65}'.format('\t'+prt_vmem_free)
            prt_vmem_active = '{:65}'.format('\t'+prt_vmem_active)
            prt_vmem_inactive = '{:65}'.format('\t'+prt_vmem_inactive)

            return (memory_line, prt_vmem_total, prt_vmem_avail, prt_vmem_avperct,
                prt_vmem_free, prt_vmem_active, prt_vmem_inactive)



        def disk_monitor():
            disk = psutil.disk_usage('/')

            prt_disk_total = 'Total disk siez: ' + str(round(disk.total / 10**9, 2)) + ' GB'
            prt_disk_used = 'Used disk size: '.ljust(40) + str(round(disk.used / 10**9,2)) + ' GB'
            prt_disk_free = 'Free disk size: '.ljust(40) + str(round(disk.free / 10**9,2)) + ' GB'
            prt_disk_occu = 'Disk occupancy rate: '.ljust(40) + str(disk.percent) + ' %'

            disk_line = '{:=^71}'.format(' DISK ')
            prt_disk_total = '{:66}'.format(prt_disk_total+'\t')
            prt_disk_used = '{:65}'.format('\t'+prt_disk_used)
            prt_disk_free = '{:65}'.format('\t'+prt_disk_free)
            prt_disk_occu = '{:65}'.format('\t'+prt_disk_occu)

            return (disk_line, prt_disk_total, prt_disk_used, prt_disk_free, prt_disk_occu)


        cpus = cpu_monitor()
        mems = mem_monitor()
        disk = disk_monitor()
        ip_addr = get_ip_address()

        terminal_theme = '''
 _______________________________________________________________________
| Ubuntu                                                            |x]||
|"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""|
|> Christopher Columbus                                                 |
|{}|
|                                                                       |
|{}|
|                                                                       |
|> {}|
|                                                                       |
{}
|                                                                       |
|{}|
|                                                                       |
|> {}|
|                                                                       |
|{}|
|{}|
|{}|
|{}|
|{}|
|                                                                       |
|{}|
|                                                                       |
|> {}|
|                                                                       |
|{}|
|{}|
|{}|
|_______________________________________________________________________|

'''.format(ip_addr,cpus[0], cpus[1], cpus[2], 
            mems[0], mems[1], mems[2], mems[3], mems[4], mems[5], mems[6],
            disk[0], disk[1], disk[2], disk[3], disk[4]
            )

        print(terminal_theme)

        print('[nohup background process list]\n')
        print('-'*80)
        grep_byte = check_output("ps -ef | grep nohup", shell=True)
        grep_str = grep_byte.decode('utf-8')
        for each_nohup in grep_str.split('\n'):
            if each_nohup:
                if 'grep' not in each_nohup:
                    print('\n▶ {}\n'.format(each_nohup))
                    print('  Log : vim {}'.format(each_nohup.split(' >> ')[1]))
                    pid = each_nohup.split()[1]
                    next_pid = str(int(each_nohup.split()[1]) + 1)
                    print('  Kill : kill -9 {} {}'.format(pid, next_pid))
                    print('  Clean up Chrome instances : pkill chrome\n')
                    print('-'*80)
        print('\nIn order to terminate working process, please type "kill -9 pid"\n')


    elif args.christopher:
        print('''
Data process procedure on christopher:

    [ Initialize ] --> [ Collect ] --> [ Render ]


    ▶ Ininitalizing
     ___________________
    |   [Initializer]   |
    |-------------------|
    | Gen batch seq     |
    | DB insert         |
    |___________________|

    ▶ Collecting Procedure : 
     ________________________________________________________
    |   [Collecting Producer]   =>   [Collecting Consumer]   |
    |----------------------------+---------------------------|
    | Query to get all seqs      | SQS receive_message       |
    | Grouping seqs as batch     | Message body to json      |
    | Batch to be SQS entry      | Request with seq number   |
    | SQS send_message_bulk      | Extract data from xml     |
    |                            | Convert xml to JSON       |
    |                            | DB insert                 |
    |____________________________|___________________________|

    ▶ Rendering Proceduer :
     _____________________________________________________
    |  [Rendering Producer]   =>   [Rendering Consumer]   |
    |--------------------------+--------------------------|
    | Query collected data     | SQS receive_message      |
    | Data preprocessing       | Message body to json     |
    | > Check producer log     | Get rendered DOM         |
    | Convert DF to list       | Upload HTML file to S3   |
    | Grouping list as batch   |                          |
    | Batch to be SQS entry    |                          |
    | SQS send_message_bulk    |                          |
    |__________________________|__________________________|
        ''')



#s3 = boto3.resource('s3')
#bucket = s3.Bucket('uneed-columbus')
#for obj in bucket.objects.filter(Prefix='html_index/'):
#    if obj.key != 'html_index/':
#        key = obj.key
#        body = obj.get()['Body'].read()
#        print(key)

    else:
        print("["+datetime_val+"] "+"arguments including 'job','action','actiondetail' need to be provided altogether!")
