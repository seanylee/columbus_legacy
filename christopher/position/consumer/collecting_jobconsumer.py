import json
import xmltodict
import boto3
import random
import time
import requests 
import sys,os
import xml.etree.ElementTree as ET
import datetime
sys.path.append(
    os.path.dirname(os.path.abspath(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)))
from meta.config import SQS
# from meta.database import session, Base, engine
from meta.models import session, Base, engine, PublicDataList, PublicDataDetail


class PubUrl:
    def __init__(self):

        self.__TEMPLATE = ('http://apis.data.go.kr/1130000/'
                         'MllInfoService/getMllInfoDetail'
                         '?seq={}&ServiceKey={}')

        self.__KEY = [
            ('N2HrBploz4wlG0wEqGbdJWaQ%2FPpdiJ819jqa4O35o%2FuyML'
                'lmwY8BH9yU4wG8mkbAh%2F1f7kDW1Gfv1oxk70Dh%2BA%3D%3D'),
            ('7WQIYmLZczGG2npwM2X7WpyjmB2210WqM9z1O9SSUrwcUovJNP'
                'KfWlkIPCSMwPCw97bQeO7Z6vtf7tSUKfk64Q%3D%3D')]

    def assemble(self, sequence):
        key         = random.choice(self.__KEY)
        assembled   = self.__TEMPLATE.format(sequence, key)
        return assembled



if __name__ == '__main__':

    print("jobconsumer is current running on pid of {}".format(os.getpid()))

    sqs_cls         = SQS()
    sqs_mtd         = sqs_cls.sqs()

    sqs             = sqs_mtd[0]
    SQS_QUEUE_URL   = sqs_mtd[1]

    url_cls         = PubUrl()

    print('STARTING WORKER listening on {}'.format(SQS_QUEUE_URL))
    while 1:
        response = sqs.receive_message(
                        QueueUrl=SQS_QUEUE_URL,
                        AttributeNames=['All'],
                        MessageAttributeNames=['string'],
                        MaxNumberOfMessages=1,
                        WaitTimeSeconds=20,)

        messages = response.get('Messages', [])
        for message in messages:

            # datetime
            datetime_val   = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            datetime_indic = '['+datetime_val+'] '
            

            #try:
            msg = message.get('Body').replace('\'','"')
            body = json.loads(msg)

            if not body.get('jobId', None):
                print(datetime_indic + 'Job Id not provided!')
                sqs.delete_message(
                    QueueUrl=SQS_QUEUE_URL, 
                    ReceiptHandle=message.get('ReceiptHandle'))

            else:
                body_sequence = body['data']['seq']
                message_comment = 'Consumer process "{}" is working on Sequence of "{}"'.format(os.getpid(), str(body_sequence))
                print(datetime_indic + message_comment)

                url_str = url_cls.assemble(body_sequence)
                r = requests.get(url_str)
                response_status = r.status_code
                xmlString = r.text


                # Extract Data from Json
                root = ET.fromstring(xmlString)
                try:
                    dmnNm = str(root.findall('body')[0][0][0].find('dmnNm').text)
                except:
                    dmnNm = None
                try:
                    wrkrNo = root.findall('body')[0][0][0].find('wrkrNo').text
                except:
                    wrkrNo = None
                try:
                    item_attrs      = [child.tag for child in root.findall('body')[0][0][0]]
                    item_attrs_cnt   = len(item_attrs)
                except:
                    item_attrs      = None
                    item_attrs_cnt   = None


                # XML to Json
                jsonString = dict(xmltodict.parse(xmlString))

                for instance in session.query(PublicDataList).filter_by(sequence=body_sequence):
                    instance.request_attempt=True
                    instance.response_code = response_status
                    instance.response_body = True

                    # UPSERT 반드시 구현 ==> seq번호가 Unique 값 이기 때문에 !!!
                    # existance = session.query(PublicDataDetail).filter_by(sequence=body_sequence)
                    # print(existance)
                    # if existance:
                    #     existance.domain=dmnNm
                    #     existance.business_no=wrkrNo
                    #     existance.res_xml=r.text
                    #     existance.res_json=jsonString
                    #     existance.item_attr_cnt=item_attrs_cnt
                    #     session.add(existance)
                    # elif not existance:



                session.add(
                    PublicDataDetail(
                        sequence=body_sequence,
                        domain=dmnNm,
                        business_no=wrkrNo,
                        res_xml=r.text,
                        res_json=jsonString,
                        item_attr_cnt=item_attrs_cnt
                    )
                )

                session.commit()
                    
                sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))
                print(
                    datetime_indic + 
                    'Consumer process "{}" for the sequence of "{}" is completed! '.format(os.getpid(), str(body_sequence)
                    +'Message has deleted from the queue')
                )
           # except Exception as e:
               # print('Exception in worker > ', e)
               # sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))

    print('WORKER "{}" STOPPED'.format(os.getpid()))

