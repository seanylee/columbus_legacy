import json
import xmltodict
import boto3
import random
import time
from datetime import datetime
from pytz import timezone

import requests
import sys,os
import xml.etree.ElementTree as ET

sys.path.append(
    os.path.dirname(os.path.abspath(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)))

from meta.config import SQS, S3
from meta.models import session, Base, engine, PublicDataList, PublicDataDetail
import subprocess
import argparse
from requests_html import HTMLSession


# ===== Datetime Define =====
date = datetime.now(timezone('Asia/Seoul')).strftime('%Y-%m-%d') # 모집 일자
datetime_val = datetime.now(timezone('Asia/Seoul')).strftime('%Y-%m-%d %H:%M:%S')

#===== Installation required ======
# sudo apt-get install libxcursor-dev:i386

#===== Chrome Dependencies =====
# sudo apt-get install gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget

# https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md


if __name__ == '__main__':

    print("[" + datetime_val + "] "+"jobconsumer is current running on pid of {}".format(os.getpid()))

    sqs_cls         = SQS(target='rendering')
    sqs_mtd         = sqs_cls.sqs()

    sqs             = sqs_mtd[0] # queue object
    SQS_QUEUE_URL   = sqs_mtd[1] # queue url

    print("[" + datetime_val + "] " + 'STARTING WORKER listening on {}'.format(SQS_QUEUE_URL))

    while True:
        response = sqs.receive_message(
                        QueueUrl=SQS_QUEUE_URL,
                        AttributeNames=['All'],
                        MessageAttributeNames=['string'],
                        MaxNumberOfMessages=1,
                        WaitTimeSeconds=20,)

        messages = response.get('Messages', [])


        for message in messages:
            # sqs message delete
            sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))
            msg = message.get('Body').replace('\'','"')
            body = json.loads(msg) # 빠른 탐색을 위해서 JSON 로드

            if not body.get('jobId', None):
                print(datetime_indic + 'Job Id not provided!')
                sqs.delete_message(
                    QueueUrl=SQS_QUEUE_URL,
                    ReceiptHandle=message.get('ReceiptHandle'))

            else:
                scheme = body['data']['scheme']
                domain = body['data']['domain']
                business = body['data']['business']

                target_url = scheme + '://' + domain # http://www.abc.com
                message_comment = 'Consumer process "{}" is rendering for the domain "{}"'.format(
                        os.getpid(), str(target_url))
                print("[" + datetime_val + "] " + message_comment)

                try:
                    with HTMLSession() as session:
                        r = session.get(target_url)
                        r.html.render(timeout=20)
                        html_byte = r.html.raw_html
                        html_utf8 = html_byte.decode("utf-8") 
                except:
                    print("Request went wrong")
                                
                s3_cls = S3()
                s3 = s3_cls.s3()

                s3_file_path = 'html_index/'
                s3_bucket_name = 'uneed-columbus'
                raw_filename = domain.replace('.','_') + '.html' # www_abc_com.html
                s3_filekey = s3_file_path + business + '-' + raw_filename # html_index/15412451-www_abc_com.html

                try:
                    s3.put_object(Body=html_utf8, Bucket=s3_bucket_name, Key=s3_filekey)
                    print("[" + datetime_val + "] " 
                            + "HTML file is successfully uploaded to s3 -> '{}'".format(raw_filename))
                except:
                    print("[" + datetime_val + "] " 
                            + "S3 upload has failed")

                # SQS 메시지 삭제
                # sqs.delete_message(QueueUrl=SQS_QUEUE_URL, ReceiptHandle=message.get('ReceiptHandle'))
