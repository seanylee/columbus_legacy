import sys, os
import json
import boto3
import uuid

sys.path.append(
    os.path.dirname(os.path.abspath(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)))
from meta.config import SQS
# from meta.database import session, Base, engine
from meta.models import PublicDataList, PublicDataDetail, session, Base, engine


def message_producing(sqs_object):
	#Base.metadata.create_all(bind=engine)
    for instance in session.query(PublicDataList).filter_by(queue_attempt=False):
        print("jobproducer grasp a job with the sequence number of '{}'".format(instance.sequence))
        body = json.dumps(
	    {'jobId': 'seqGen', 
	    'data': {'seq': str(instance.sequence)}}
	    )
        try:
            sqs.send_message(QueueUrl=SQS_QUEUE_URL,MessageBody=body)
            instance.queue_attempt=True
        except:
            None

    try:
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def batch_message_producing(sqs_object):

    batch_count = 10
    seq_list_all = [instance.sequence for instance in session.query(PublicDataList).filter_by(queue_attempt=False)]

    for batch_step in range(0,len(seq_list_all), batch_count):
        ten_elems = seq_list_all[batch_step:batch_step + batch_count]


        set_elems = set(ten_elems)
        #session.query(PublicDataList).where(PublicDataList.sequence.in_(set_elems)).values(queue_attempt=True)
        session.query(PublicDataList).filter(PublicDataList.sequence.in_(set_elems)).update({"queue_attempt": True}, synchronize_session='fetch')

#        try:
#            session.commit()
#        except:
#            session.rollback()
#            raise
#        finally:
#            session.close()
        
        entries = list()
        for enu, sequence_num in enumerate(ten_elems):
            identifier = 'msg_{}'.format(enu)
            dict_msg = {
                        'Id': identifier, 
                        'MessageBody': str({"jobId": "seqGen", "data": {"seq": str(sequence_num)}})
                    }
            entries.append(dict_msg)

        #try:
        sqs.send_message_batch(
            QueueUrl    = SQS_QUEUE_URL,
            Entries     = entries
        )
        #except:
           # print('sqs messages not produced > {}'.format(entries))
        print('producer process is over')

        try:
            session.commit()
        except:
            session.rollback()
            raise

    session.close()

if __name__=='__main__':
    print("jobproducer is current running on pid of {}".format(os.getpid()))
    sqs_cls 		= SQS()
    sqs_mtd 		= sqs_cls.sqs()
    sqs 		= sqs_mtd[0]
    SQS_QUEUE_URL 	= sqs_mtd[1]


    # sys.argv ==> ['./position/producer/jobproducer.py', 'collecting', 'now']
    action              = sys.argv[1] # 가령, singlerun
    action_detail       = sys.argv[2] # 가령, now

    #message_producing(sqs) 
    if action == 'collecting':
        batch_message_producing(sqs)

