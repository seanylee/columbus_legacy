import sys, os
import json
import boto3
import uuid

import pandas as pd
import numpy as np
from urlextract import URLExtract
import requests
from time import sleep
from urllib.parse import urlparse


sys.path.append(
    os.path.dirname(os.path.abspath(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)))
from meta.config import SQS
# from meta.database import session, Base, engine
from meta.models import PublicDataList, PublicDataDetail, session, Base, engine


# 배치 그룹 생성 (n값의 갯수 만큼 배치)
def to_batch(target_list, batch_size=20):
    print("expected batch size = {}".format(batch_size))
    
    # Init
    batch_group = list()
    batch_1 = list()
    
    for i in range(0, len(target_list), batch_size):
        if len(target_list[i:i+batch_size]) == batch_size:
            batch_group.append(target_list[i:i+batch_size])
        else:
            batch_1.append(target_list[i:i+batch_size])
    return batch_group, batch_1




def batch_message_producing(sqs_object):
    # ================= DATA EXTRACT FROM DATABASE ====================
    record_list = [
            [inst.sequence,inst.domain,inst.business_no] 
            for inst in session.query(PublicDataDetail).filter(PublicDataDetail.domain.isnot(None))]

    # ================== DATA TRANSFORM WITHIN NUMPY DATAFRAME=================== 
    # transform listed list into numpy data frame
    df = pd.DataFrame(record_list, columns = ['sequence','domain','business_no'])
    
    # URLExtractor 사용해서 URL 추출
    extractor = URLExtract()
    df['clean_domain'] = df.apply(lambda df : extractor.find_urls(df['domain']), axis=1)

    # 도메인 개수 파악
    df['cardinality'] = df.apply(lambda df : len(df['clean_domain']), axis=1)

    # 도메인이 1개 이상 존재하는 로우 조건 추가
    df = df[(df.cardinality >= 1)]

    # 첫번째 인덱스 도메인 추출
    df['clean_domain'] = df['clean_domain'].str[0]

    # 프로토콜 추가
    df.loc[df['clean_domain'].str.startswith('http')==False, 'clean_domain'] = 'http://' + df['clean_domain']

    # 데이터프레임에서 불필요한 컬럼 삭제
    df = df.drop(['domain','cardinality'], axis=1)

    # URL 파싱 -> scheme, netloc
    df['scheme'] = df.apply(lambda df : urlparse(df['clean_domain']).scheme, axis=1)
    df['netloc'] = df.apply(lambda df : urlparse(df['clean_domain']).netloc, axis=1)

    # 도메인(netloc) 중복(Duplication) 모두 제거
    df = df.drop_duplicates(['netloc'], keep=False)

    # 사업자등록번호 중복(Duplication) 모두 제거
    df = df.drop_duplicates(['business_no'], keep=False)

    # dataframe은 남겨둔 채 index.html을 가져오기 위한 작업을 위해 데이터 형식 변환
    linklist = list()
    for grouped in df[['scheme','netloc','business_no']].values.tolist():
        linklist.append(grouped)

#    print(linklist)
    batched = to_batch(linklist,batch_size=10)
    batch_group = batched[0]
    try:
        batch_1 = batched[1][0]
    except:
        batch_1 = batched[1]


    #entries = list()

    for batch in batch_group:
        entries = list()
        #['http', 'ugshop.co.kr', '1322100715']
        for each in batch:
            a_scheme = each[0]
            a_domain = each[1]
            a_business = each[2]

            dict_msg = {
                    'Id': '{}'.format(a_business),
                    'MessageBody': str(
                                {
                                    "jobId": "RenderGen",
                                    "data": {
                                            "scheme": a_scheme,
                                            "domain": a_domain,
                                            "business": a_business
                                        }
                            }
                        )

            }
            entries.append(dict_msg)

        sqs.send_message_batch(
                QueueUrl = SQS_QUEUE_URL,
                Entries = entries
        )

        try:
            session.commit()
        except:
            session.rollback()
            raise

    batch_1_entries = list()
    for each in batch_1:
        a_scheme = each[0]
        a_domain = each[1]
        a_business = each[2]

        dict_msg = {
            'Id': '{}'.format(a_business),
            'MessageBody': str(
                        {
                            "jobId": "RenderGen",
                            "data": {
                                        "scheme": a_scheme,
                                        "domain": a_domain,
                                        "business": a_business
                                    }
                        }
                    )
                }
        batch_1_entries.append(dict_msg)

    print(batch_1_entries)
    sqs.send_message_batch(
            QueueUrl = SQS_QUEUE_URL,
            Entries = batch_1_entries
    )

    try:
        session.commit()
    except:
        session.rollback()
        raise


    session.close()        
    print('producer process is over')





if __name__=='__main__':
    print("jobproducer is current running on pid of {}".format(os.getpid()))
    sqs_cls             = SQS(target='rendering')
    sqs_mtd             = sqs_cls.sqs()
    sqs                 = sqs_mtd[0]
    SQS_QUEUE_URL       = sqs_mtd[1]


    # sys.argv ==> ['./position/producer/jobproducer.py', 'singlerun', 'now']
    action              = sys.argv[1] # 가령, singlerun
    action_detail       = sys.argv[2] # 가령, now

    #message_producing(sqs) 
    if action == 'rendering':
        batch_message_producing(sqs)
