import os,sys
sys.path.append(
    os.path.dirname(os.path.abspath(
        os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)))
import requests
import random
# from meta.database import session, Base, engine
from meta.models import PublicDataList, PublicDataDetail, session, Base, engine


from datetime import datetime
import datetime

import xml.etree.ElementTree as ET

"""
Todo 
Report 규칙 제정하기 || 언제 기록에 남길것인가? 무엇을 기록에 남길 것인가?
"""


class initialize:

    """
    Sailor Registered for the work "Initialization"
    """

    def __init__(self):

        self.date           = datetime.datetime.now().strftime('%Y-%m-%d')
        self.datetime_val   = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


    def initialization_log(self, event):

        datetime_indic = '['+self.datetime_val+'] '
        
        if event == 'singlerun':
            journal_ = datetime_indic+"Sailor just started 'initializer-singlerun'"

        if event == 'dailyrun':
            journal_ = datetime_indic+"Sailor just started 'initializer-dailyrun'"

        elif event == 'singlerun_complete':
            journal_ = datetime_indic+"Sailor just finished 'initializer-singlerun'"

        elif event == 'fail_initializer':
            journal_ = datetime_indic+"Sailor's action(method) 'range_initializer' failed"

        elif event == 'batch_dumping_exception':
            journal_ = datetime_indic+"Initialization batch dumping exception"

        elif event == 'before_action_report':
            journal_ = datetime_indic+"Sailor is inserting data record! Target total : {}"

        else:
            journal_ = 'Unknown'

        return journal_


    def singlerun(self, end_seq, init=True):
        """
        Singlerun ==> Generates sequence number all the way upto 700,000.
        """
        end_sequence    = (int(end_seq) + 1)
        confirm         = init

        self.range_initializer(end_seq=end_sequence, init=confirm)
        print(self.initialization_log('singlerun'))



    def dailyrun(self, yesterday):
        """
        Get business sequence number, which has registered yesterday.
        """

        url = ("http://apis.data.go.kr/1130000/MllInfoService/"
                +"getMllSttemntInfo?&fromPermYmd={dateval}&toPermYmd={dateval}&ServiceKey=")

        key = [
                ('N2HrBploz4wlG0wEqGbdJWaQ%2FPpdiJ819jqa4O35o%2FuyML'
                    'lmwY8BH9yU4wG8mkbAh%2F1f7kDW1Gfv1oxk70Dh%2BA%3D%3D'),
                ('7WQIYmLZczGG2npwM2X7WpyjmB2210WqM9z1O9SSUrwcUovJNP'
                    'KfWlkIPCSMwPCw97bQeO7Z6vtf7tSUKfk64Q%3D%3D')
            ]


        getkey      = random.choice(key)
        full_url    = (url.format(dateval=yesterday) + getkey)
        req         = requests.get(full_url)

        root        = ET.fromstring(req.text)
        seq_list    = [item.find('seq').text for item in root.findall('body')[0][0]]
        
        self.batch_initializer(sequence_lsit=seq_list, init=True)
        print(self.initialization_log('dailyrun'))




    def range_initializer(self, start_seq=1, end_seq=700000, step=100000, init=False):
        """
        Seq 값의 Duplication 방지를 보장. start_seq 부터 end_seq까지 순회하며 step 수 만큼 배치덤프 생성
        """

        def batch(iterable, n=1): # 배치 덤프 생성 // n 만큼 하나의 배치로
            l = len(iterable)
            for ndx in range(0, l, n):
                yield iterable[ndx:min(ndx + n, l)]

        if init:
            # try:
            Base.metadata.create_all(bind=engine)

            for batch_dump in batch(range(int(start_seq), int(end_seq)), int(step)):

                print(batch_dump)
                listbatch = [PublicDataList(sequence=dump_) for dump_ in batch_dump]
                session.add_all(listbatch)
                session.commit()
            print(self.initialization_log('singlerun_complete'))

            # except:
            #     print(self.initialization_log('batch_dumping_exception'))

        else:
            print(self.initialization_log('fail_initializer'))





    def batch_initializer(self, sequence_lsit, init=False):
        
        if init:
            # Base.metadata.create_all(bind=engine)
            print(self.initialization_log('before_action_report').format(str(len(sequence_lsit))))
            listbatch = [PublicDataList(sequence=dump_) for dump_ in sequence_lsit]
            session.add_all(listbatch)
            session.commit()

        else:
            print(self.initialization_log('fail_initializer'))




if __name__=='__main__':
    
    # 안전하게 else 처리 꼭!
    # INCOMING ==> ['./position/initializer/jobinitializer.py', 'singlerun', 'now']
    action          = sys.argv[1] if sys.argv[1] else 'action_not_provided'
    actiondetail    = sys.argv[2] if sys.argv[2] else 'actiondetail_not_provided'
    

    print("jobinitializer is current running on pid of {}".format(os.getpid()))


    # initiate class!
    init_cls = initialize()
    if action == 'singlerun':
        if actiondetail.isdecimal():
            init_mtd = init_cls.singlerun(end_seq=actiondetail)

    elif action == 'dailyrun':
        if actiondetail == 'actiondetail_not_provided': # Yesterday
            today = datetime.date.today()
            actiondetail = (today - datetime.timedelta(days = 1)).strftime('%Y%m%d')


        # Input 값으로 받을때 날짜 형식 예외처리
        # 대상 : '2019-01-01', '2019.01.01', '2019/01/01'
        elif '-' in actiondetail:
            actiondetail = actiondetail.replace('-','')
        elif '/' in actiondetail:
            actiondetail = actiondetail.replace('/','')
        elif '.' in actiondetail:
            actiondetail = actiondetail.replace('.','')

        init_cls.dailyrun(actiondetail)
        
    else:
        print("Not defined action name '{}'".format(action))
    # elif action == 'intervalrun':
        # None

    





